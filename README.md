# Side Dishes

This TCoAaL mod adds about an hour of content to episode 1. It's recommended to start a new game to experience most of it.

## Installation

You can [download the latest Side Dishes mod release here](https://codeberg.org/canopy/sidedishes/releases). To play this mod an installation of [Tomb](https://codeberg.org/basil/tomb) is required. Installation instructions for Tomb can be found in the [modding wiki](https://coffin-wiki.basil.cafe/playing/install-a-modloader#prebuilt). With Tomb installed, you can copy the sidedishes folder into the `/tomb/mods` folder. This will cause the mod to be automatically load during the next start of the game. The file structure should look similar to this, where the sidedishes folder can (but doesn't have to) be a zip file:
```
The Coffin of Andy and Leyley/
├── tomb/
│   ├── index.html
│   ├── mods/
│   │   ├── sidedishes/
│   │   │	└── canopyimagebuilder/
│   │   │	└── data/
│   │   │	└── img/
│   │   │	└── languages/
│   │   │	└── mod.json
│   │   │	└── README.md
│   │   │	└── sidedishes_resources.json
│   │   └── tomb/
│   └── tomb/
```

### Troubleshooting	

- **Some images aren't loading** if the mod folder or zip file is renamed. That's because a plugin, which creates images based on original game assets when the game starts, expects the mod folder to be named either `sidedishes` or `sidedishes.zip`. If it's renamed the images will be linked incorrectly and won't be displayed.

- The **official Fan Translation** project is currently **not supported**. If you use it, you'll have to disable it to play this mod. Exceptions are languages listed under [Translations](#chaptranslations). These require additional installation steps, which are explained by the author of the Translation in the corresponding repository.

## Credits

Special thanks to...
- Iceblaster for reading my raw, shoddy writing so you don't have to. This mod is better due to the revisions she suggested.
- an artist who prefers to remain anonymous for drawing the nurse sprites for my mod and letting me modify them a bit.
- [u/MR-E-Graves](https://www.reddit.com/user/MR-E-Graves/) for granting me permission to use his [Shopkeeper Busts](https://www.reddit.com/r/CoffinofAndyandLeyley/comments/1afqcsd/the_shopkeeper_didnt_have_a_portrait_so_i_made/).

## Disclaimer

Generative AI has been used to create images for this mod.
Generative AI has been used to write code for this mod.

## Translations <a name="chaptranslations"></a>

Translations of this mods can be found listed below. I'm not involved in their development or maintenance. 

[Portuguese (Brazilian) - Português (Brasil)](https://codeberg.org/Palazzo/side-dishes-pt-brasil) by Palazzo
[Russian - Русский](https://codeberg.org/TheVorkMan/sidedishes-russiantranslation) by TheVorkMan

## Things I could use help with

- Art matching the style of the game.
- please report any bugs you run into so I can fix them!
- I'd like to add some miscellaneous interactions with objects throughout episode 1. If you have some ideas for 1- or 2-liners, feel free to share them!
- You can reach out to me via Discord, Reddit or Codeberg issue.

## Changelog for v.0.5.5

- fixed a bug which allowed Ashley to step onto the door to the hallway early in the game.
- minor corrections in dialogue and labels.
- added link to pt-BR translation.
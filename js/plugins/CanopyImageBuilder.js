/*:
* @plugindesc This plugin uses original game images to assemble and patch new images.
* @author canopy
*
*
* @param Settings
* @desc Contains all of the plugin settings
*
*
* @param Input Subdirectory
* @desc This folder contains information about how the images are to be build. Namely Instruction JSONs, masks and patches.
* @type text
* @default \canopyimagebuilder\input
* @parent Settings
*
*
* @param Output Subdirectory
* @desc This folder contains 
* @type text
* @default \canopyimagebuilder\output
* @parent Settings
*
*
* @param Target Images Directory
* @desc Filepath to the folder which contains the target images, which are used to calculate patches for the base images
* @type text
* @default S:\TCOAAL\image selection\ForCanopyImageBuilder\targets
* @parent Settings
*
*
* @help
* Can assemble base images based on game assets.
* Can create patch images based on a target image and a base image.
* Upon loading Images in the game, this plugin redirects urls of the images it manges to the corresponding files in the output folder.
*
*/

(function() {
  const fs = require('fs');
  const path = require('path');
  const zlib = require('zlib');
  const { execSync } = require('child_process');

  CanopyImageBuilderParams = PluginManager.parameters("CanopyImageBuilder");

  var canopyIB = {}
  canopyIB.isInDevEnvironment = isInDevEnvironment();
  canopyIB.isMod = false;
  
  const CONSOLELOG = {
    DEFAULT: 0,
    DEBUG: 1,
    IMG_LOADING: 2,
  }
  canopyIB.consoleLog = [
    CONSOLELOG.DEFAULT
  ];
  

  if (CanopyImageBuilderParams["Output Subdirectory"] == undefined){
    //console.log("CanopyImageBuilderParams didn't load");
    CanopyImageBuilderParams = {
      "Output Subdirectory":  "/tomb/mods/sidedishes/canopyimagebuilder/output",
      "Input Subdirectory":   "/tomb/mods/sidedishes/canopyimagebuilder/input",
      "Target Images Directory": ""
    }
    canopyIB.isInDevEnvironment = false;
    canopyIB.isMod = true;
  }

  canopyIB.paths = {};
  canopyIB.paths.projectRoot = process.cwd().replace(/\\/g, "/"); //replace all "\" with "/" characters  
  canopyIB.paths.output  = ("" + canopyIB.paths.projectRoot + CanopyImageBuilderParams["Output Subdirectory"]).replace(/\\/g, "/");  //output of the finished images for usage in the modded game. Identical to cibDirs.output for the finished mod.
  canopyIB.paths.input   = ("" + canopyIB.paths.projectRoot + CanopyImageBuilderParams["Input Subdirectory"]).replace(/\\/g, "/");   //build instructions of the mod.
  canopyIB.paths.targets  = ("" + CanopyImageBuilderParams["Target Images Directory"]).replace(/\\/g, "/");  //target images used to calculate patches are stored here. This folder should be removed before shipping the mod.
  canopyIB.paths.folderWithEncryptedAssets;
  if (canopyIB.isInDevEnvironment) {
    canopyIB.consoleLog = [
      CONSOLELOG.DEBUG
      //CONSOLELOG.IMG_LOADING
    ];

    deleteFolderRecursiveSync(canopyIB.paths.projectRoot + "/canopyimagebuilder");
    canopyIB.paths.inputDev = getFolderPathNextToPathByName(canopyIB.paths.targets, "inputDev");
    canopyIB.paths.outputDev = getFolderPathNextToPathByName(canopyIB.paths.targets, "outputDev");
    canopyIB.paths.debug = getFolderPathNextToPathByName(canopyIB.paths.targets, "debug");
    canopyIB.paths.folderWithEncryptedAssets = getFolderPathNextToPathByName(canopyIB.paths.targets, "encryptedGameAssets");
    /* TODO: Look into this!
    * for whatever reason the game doesn't want read encrypted files in the playtest environment anymore, 
    * after I've added a custom PIXI loader to process non-game assets. 
    * It still does though when run with the same script as a mod... bizarre...
    * give it the unencrypted Data instead, I suppose...
    * //canopyIB.paths.folderWithEncryptedAssets = getFolderPathNextToPathByName(canopyIB.paths.targets, "unencryptedGameAssets");
    * EDIT: turns out it wants both?! I should probably look into this, but lets just provide both for now:
    */
    canopyIB.paths.folderWithEncryptedAssets = getFolderPathNextToPathByName(canopyIB.paths.targets, "unencryptedAndEncryptedGameAssets");
    createSubDir(canopyIB.paths.projectRoot, canopyIB.paths.input.slice(canopyIB.paths.projectRoot.length));
    createSubDir(canopyIB.paths.projectRoot, canopyIB.paths.output.slice(canopyIB.paths.projectRoot.length));
  } else if (canopyIB.isMod){
    canopyIB.paths.folderWithEncryptedAssets = "" + canopyIB.paths.projectRoot + "/www";
    //canopyIB.paths.debug = getFolderPathNextToPathByName(canopyIB.paths.input, "debug");
    setupSidedishesResourcesModIfModFolderIsZipped(); 
  }

  canopyIB.do = {};
  canopyIB.do.outputLoadedImagesInDebugFolderIfInDevEnvironment = true;
  canopyIB.do.logPathUponExportingImageAsPng = false;
  canopyIB.do.calculatePatches = canopyIB.isInDevEnvironment;
  canopyIB.do.applyPatches = true;
  canopyIB.do.saveOutputInGameImgFolder = true;
  //canopyIB.do.deleteOutputInGameImgFolder = false;//(!canopyIB.do.saveOutputInGameImgFolder);
  if (showlog(CONSOLELOG.DEBUG)) {console.log("canopyIB: ", canopyIB)};

  /* process stuff */
  var onLoadBitmapMV = function(instructionFileName, bmpNameWithoutExtension, bmp){
    if (showlog(CONSOLELOG.IMG_LOADING)) {console.log("For instruction file \"" + instructionFileName + "\" MV has loaded \"" 
      + bmpNameWithoutExtension + "\": ", bmp)};
    //console.log("onLoadBitmapMV bmp \"" + bmpNameWithoutExtension + "\": ", bmp)
    let bmpImgData = bmp.canvas.getContext("2d").getImageData(0,0,bmp.canvas.width,bmp.canvas.height);
    if (canopyIB.do.outputLoadedImagesInDebugFolderIfInDevEnvironment && canopyIB.isInDevEnvironment){
      let outputPath = canopyIB.paths.debug + "/loaded_assets/" + instructionFileName + "_loaded_" + bmpNameWithoutExtension + ".png";
      let reconstructedBmpCanvas = generateCanvasWithImageDataData (bmpImgData.data, bmpImgData.width, bmpImgData.height);
      exportCanvasAsImgFile(reconstructedBmpCanvas, outputPath);
    }
    addImgDataToInstructionFiles(instructionFileName, bmpNameWithoutExtension, bmpImgData);  
    //ImageManager.clearRequest();
  }

  canopyIB.paths.pathToFolderWithInstructionFiles = canopyIB.paths.input
  if (canopyIB.isInDevEnvironment) {
    canopyIB.paths.pathToFolderWithInstructionFiles = canopyIB.paths.inputDev;
  }
  canopyIB.instructionFileNames = collectInstructionFileNames(canopyIB.paths.pathToFolderWithInstructionFiles);
  canopyIB.instructionFiles = {};
  canopyIB.BitmapRequestData = {
    fileNames: [],
    fileSubPaths: [],
  }
  canopyIB.BitmapRequestData.folderPath = canopyIB.paths.output;
  canopyIB.BitmapRequestData.imgagesCanBeLoaded = false;
  if (isInDevEnvironment){
    canopyIB.BitmapRequestData.folderPath = canopyIB.paths.outputDev;
  }
  canopyIB.pixiLoader = new PIXI.loaders.Loader();
  canopyIB.pixiLoader.onComplete.add(() => {
    console.log("canopyIB.pixiLoader.onComplete.add(() function has been called.");
  });
  //collect instruction files and compute some metadata needed to process them
  for (let instructionFileName of canopyIB.instructionFileNames){
    copyFileFromDevInputFolderToRegularInputfolderIfIsInDevEnvSync(instructionFileName);
    let instructionFilePath = "" + canopyIB.paths.pathToFolderWithInstructionFiles + "/" + instructionFileName;
    let instructionFile = loadInstructionFile(instructionFilePath);
    let loadPatch = false;
    let loadTarget = false;
    if (canopyIB.do.calculatePatches){
      if (instructionFile.patch != null){
        loadTarget = fs.existsSync(""+ canopyIB.paths.targets + "/" + instructionFile.patch["targetFileName"] );
      }   
    }
    if (canopyIB.do.applyPatches){
      if (instructionFile.patch != null){
        loadPatch = fs.existsSync("" + canopyIB.paths.input + "/" + instructionFile.patch["patchFileName"]);   
      }
    }
    //add imgData to the instructionFile, which the images will be stored in after they've been loaded.
    instructionFile.name = path.parse(instructionFileName).name;
    instructionFile.loadPatch = loadPatch;
    instructionFile.loadTarget = loadTarget;
    instructionFile.imgData = {}
    for (let key in instructionFile.sourceImages){
      instructionFile.imgData[key] = null;
    }
    if (instructionFile.masks != null){
      if (!(JSON.stringify(instructionFile.masks) === '{}')){
        for (let key in instructionFile.masks){
          instructionFile.imgData[key] = null;
          copyFileFromDevInputFolderToRegularInputfolderIfIsInDevEnvSync(instructionFile.masks[key]);
        }
      }
    }
    if ((instructionFile.patch != null) && (loadTarget)){
      instructionFile.imgData[instructionFile.name + "_target"] = null;
    }
    canopyIB.instructionFiles[instructionFile.name] = instructionFile;
    canopyIB.BitmapRequestData.fileNames.push(instructionFile.name + ".png");
    canopyIB.BitmapRequestData.fileSubPaths.push(instructionFile.outputDestination + instructionFile.name + ".png");
  }
  let jsonFilePath = canopyIB.paths.input + "/" + "!_managedImages.json"; 
  const jsonString = JSON.stringify(canopyIB.BitmapRequestData.fileSubPaths, null, 2);
  fs.writeFile(jsonFilePath,jsonString, (err) => {if (err) {console.error("Error writing file", err);}});
  if (canopyIB.isInDevEnvironment){
    jsonFilePath = canopyIB.paths.inputDev + "/" + "!_managedImages.json"; 
    fs.writeFile(jsonFilePath,jsonString, (err) => {if (err) {console.error("Error writing file", err);}});
  }
  requestToProcessNextInstructionFile();

  /* Hooks*/
  var canopyImageBuilderBitmap__requestImage = Bitmap.prototype._requestImage;
  Bitmap.prototype._requestImage = function(url){
    let noneOfMyBusiness = true;
    if (canopyIB.BitmapRequestData.imgagesCanBeLoaded){
      for (let fileName of canopyIB.BitmapRequestData.fileNames){
        if (url.endsWith(fileName)){
          let originalUrl = url;
          if (canopyIB.isMod){
            console.log("redirecting url:", {
              ismod: canopyIB.isMod,
              url: url,
              newUrl: canopyIB.paths.output + "/" + fileName,
            })
            url = canopyIB.paths.output + "/" + fileName;
          } else {
            console.log("redirecting url:", {
              ismod: canopyIB.isMod,
              url: url,
              newUrl: canopyIB.BitmapRequestData.folderPath+ "/" + fileName,
            })
            url = canopyIB.BitmapRequestData.folderPath+ "/" + fileName;
          }
          if(Bitmap._reuseImages.length !== 0){
            this._image = Bitmap._reuseImages.pop();
          }else{
              this._image = new Image();
          }
    
          if (this._decodeAfterRequest && !this._loader) {
            //this._loader = ResourceHandler.createLoader(url, this._requestImage.bind(this, url), this._onError.bind(this)); //orig
            this._loader = ResourceHandler.createLoader(originalUrl, Bitmap.prototype._requestImage.bind(this, originalUrl), this._onError.bind(this));
            //console.log("this._loader: ", this._loader);
          }      
    
          this._image = new Image();
          this._url = originalUrl; //TODO original url or redirected url here?
          this._loadingState = 'requesting';
    
          this._image.src = url;
    
          this._image.addEventListener('load', this._loadListener = Bitmap.prototype._onLoad.bind(this));
          this._image.addEventListener('error', this._errorListener = this._loader || Bitmap.prototype._onError.bind(this));
          noneOfMyBusiness = false;
          break
        }
      }
    }
    if (noneOfMyBusiness) {
      canopyImageBuilderBitmap__requestImage.call(this, url);
    }
  }

  /* Functions */
  function showlog(consoleLogSetting){
    let thisShallBePrinted = false;
    for (let chosenSetting of canopyIB.consoleLog){
      if (chosenSetting == consoleLogSetting){
        thisShallBePrinted = true;
        break
      }
    }
    return thisShallBePrinted;
  }

  function isInDevEnvironment(){
    let targetDir = CanopyImageBuilderParams["Target Images Directory"];
    if (targetDir != null) {
      if (targetDir !== ""){
        return true;
      }
    }
    return false;
  }

  function getFolderPathNextToPathByName(path, dirName){
    //console.log("path: " + path);
    let pathArray = path.split("/");
    //console.log("pathArray: " + pathArray);
    let lastElementOffet;
    if (pathArray[pathArray.length -1] === "") {
      lastElementOffet = -1;
    } else {
      lastElementOffet = 0
    }
    pathArray[pathArray.length -1 + lastElementOffet] = dirName;
    path = pathArray[0];
    for (let i = 1; i < pathArray.length + lastElementOffet; i++){
      path = path + "/" + pathArray[i];
    }
    return path;
  }

  /**
   * Creates new directories in projectPath to complete the subpath newDir.
   * 
   * If sourceDir is provided an empty folder structure will be copied from sourceDir into "projectPath/newDir/"
   * @param {String} projectPath 
   * @param {String} newDir 
   * @param {String} sourcePath 
   */
  function createSubDir(projectPath, newDir, sourcePath) {
    sourcePath = sourcePath || null;

    let subDirs = newDir.split("/");
    if (subDirs.length < 1) {throw new Error("Invalid folder name: Folder names cannot be empty");}
    let folderPath = ("" + projectPath);
    for(let folderName of subDirs){
      folderPath = ("" + folderPath + "/" + folderName);
      if (!fs.existsSync(folderPath)) {
        fs.mkdirSync(folderPath);
      }
    }
    
    if (!sourcePath) { return };

    let sourceSubDirs = fs.readdirSync(sourcePath, { withFileTypes: true });
    for(let folderName of sourceSubDirs){
      let srcSubDir = ("" + folderPath + "/" + folderName);
      if (!fs.existsSync(srcSubDir)) {
        fs.mkdirSync(srcSubDir);
      }
    }

  }

  function setupSidedishesResourcesModIfModFolderIsZipped(){
    let modDirPath = canopyIB.paths.projectRoot + "/tomb/mods/sidedishes";
    //if(true){ //for debugging as playtest
    if (!fs.existsSync(modDirPath) || !fs.lstatSync(modDirPath).isDirectory()){
      let modResourcePath = canopyIB.paths.projectRoot + "/tomb/mods/sidedishes_resources";
      canopyIB.paths.output  = modResourcePath + "/canopyimagebuilder/output";  //output of the finished images for usage in the modded game. Identical to cibDirs.output for the finished mod.
      canopyIB.paths.input   = modResourcePath + "/canopyimagebuilder/input";   //build instructions of the mod.
  
      const zipPath = canopyIB.paths.projectRoot + "/tomb/mods/sidedishes.zip"
      if (!fs.existsSync(zipPath)){
        throw new Error("The sidedishes mod needs to be either a zip or a folder named \"sidedishes\" at the following location: ${zipPath}");
      }
      //if mod isn't a folder assume it's a zip file and create dirs to build the images;    
      extractZip(zipPath, modResourcePath);
      deleteFolderRecursiveSync(modResourcePath + "/data");
      deleteFolderRecursiveSync(modResourcePath + "/img");
      deleteFolderRecursiveSync(modResourcePath + "/js");
      deleteFolderRecursiveSync(modResourcePath + "/languages");
      fs.unlinkSync(modResourcePath + "/mod.json");
      fs.unlinkSync(modResourcePath + "/README.md");
      fs.renameSync(modResourcePath + "/sidedishes_resources.json", modResourcePath + "/mod.json");
      
      function extractZip(zipFilePath, extractToPath) {
          // Create the destination folder if it doesn't exist
          if (!fs.existsSync(extractToPath)) {
              fs.mkdirSync(extractToPath, { recursive: true });
              //console.log(`Folder created at: ${extractToPath}`);
          }
      
          // Use the 'unzip' command to extract the ZIP file
          try {
              execSync(`tar -xf "${zipFilePath}" -C "${extractToPath}"`); //natively supported by Windows, Linux and Mac according to ChatGPT
              //execSync(`unzip -o "${zipFilePath}" -d "${extractToPath}"`); //not natively availabale on Windows according to ChatGPT
              //console.log(`Extracted to: ${extractToPath}`);
          } catch (err) {
              console.error(`Error extracting ZIP file: ${err.message}`);
          }
      }
    }
  }

  function deleteFolderRecursiveSync(folderPath) {
    if (fs.existsSync(folderPath)) {
        fs.readdirSync(folderPath).forEach((file) => {
            const currentPath = path.join(folderPath, file);
            if (fs.lstatSync(currentPath).isDirectory()) {
                deleteFolderRecursiveSync(currentPath);
            } else {
                fs.unlinkSync(currentPath);
            }
        });
        fs.rmdirSync(folderPath);
        //console.log(`Deleted folder: ${folderPath}`);
    } else {
        console.log(`Folder not found: ${folderPath}`);
    }
}

  function loadInstructionFile(filePath) {
    try {
        const data = fs.readFileSync(filePath, 'utf-8');
        const jsonData = JSON.parse(data);
        return jsonData;
    } catch (err) {
        console.error("Error reading or parsing the JSON file \"" + filePath + "\"", err);
        return null;
    }
  }

  function collectInstructionFileNames(buildDir) {
    let files = fs.readdirSync(buildDir);
    let fileNameList = [];
    for (var i = 0; i < files.length; i++) {
        let filename = files[i];
        if (filename.endsWith(".json")) {
          if (filename != "!_managedImages.json"){
            fileNameList.push(filename);
          }
        };
    };
    return fileNameList;
  };

  function generateCanvasWithImageDataData (imgDataData, width, height){
    let canvas = document.createElement('canvas');
    let context = canvas.getContext('2d');
    canvas.width = width;
    canvas.height = height;
    context.clearRect(0, 0, width, height);
    let imgData = new ImageData(imgDataData, width, height);
    context.putImageData(imgData, 0, 0);
    return canvas;
  }

  function exportCanvasAsImgFile(canvas, outputPath) {
    
    let pathArray = outputPath.split("/");
    let fileNameArray = pathArray[pathArray.length -1].split(".");
    let fileType = "png";
    if (fileNameArray.length > 1){
      fileType = fileNameArray[fileNameArray.length -1];
    }
    let imageData = canvas.toDataURL("image/" + fileType, 1);
    let base64Data = imageData.replace(/^data:image\/png;base64,/, '');
    fs.writeFile(outputPath, base64Data, "base64", function (err) {
      if (err) {
        console.error("Error saving image to: " + outputPath, err);
      } else {
        if (canopyIB.do.logPathUponExportingImageAsPng){
          console.log("Image saved to " + outputPath);
        }
      }
    });
  }

  function exportPatchAsBinGz(data, outputPath){
    let buffer = Buffer.from(data);
    try {
      const compressedData = zlib.gzipSync(buffer);
      fs.writeFileSync(outputPath, compressedData);
    } catch (err) {
        console.error('Error compressing or writing data:', err);
    }
  }

  function createPixiMatrix(arrayDescribingRows){
    let matrix = new PIXI.Matrix();
    matrix.set(arrayDescribingRows[0], arrayDescribingRows[3], arrayDescribingRows[1], arrayDescribingRows[4], arrayDescribingRows[2], arrayDescribingRows[5]);
    return matrix;
  }

  function calcBoundingRect(imgData){
    let minX = imgData.width;
    let minY = imgData.height;
    let maxX = 0;
    let maxY = 0;
    const numberOfChannels = imgData.data.length / (imgData.width * imgData.height);

    for (let i = 0; i < imgData.data.length; i += numberOfChannels) {
      //i is red, i + 1 is green, i + 2 is blue, i + 3 is alpha channel; could be less if no color and/or no alpha is present in img
      let xPos = ((i + numberOfChannels) / numberOfChannels) % imgData.width;
      let yPos = Math.floor(((i + numberOfChannels) / numberOfChannels) / imgData.width);
      if (imgData.data[i] > 0){
        //if first channel > 0, we've got a point overlapping the mask
        if (xPos < minX) {minX = xPos};
        if (xPos > maxX) {maxX = xPos};
        if (yPos < minY) {minY = yPos};
        if (yPos > maxY) {maxY = yPos};
      }
    }

    return new PIXI.Rectangle(minX, minY, maxX - minX + 1, maxY - minY + 1);
  }

  async function requestToLoadUnencryptedPixiImages(instructionFile, instructionFileName, loadPatch, loadTarget) {
    try {
        loadPatch = loadPatch || false;
        loadTarget = loadTarget || false;
        if (canopyIB.pixiLoader.loading){
          if (showlog(CONSOLELOG.IMG_LOADING)) {console.log("canopyIB.pixiLoader.loading: ", canopyIB.pixiLoader.loading);}
          //canopyIB.pixiLoader.destroy();
          //canopyIB.pixiLoader = new PIXI.loaders.Loader();
          //if (showlog(CONSOLELOG.IMG_LOADING)) {console.log("PIXI loader has been destroyed and recreated");}
          //requestToLoadUnencryptedPixiImages(instructionFile, instructionFileName, loadPatch, loadTarget);

          setTimeout(function (){
  
            canopyIB.pixiLoader.destroy();
            canopyIB.pixiLoader = new PIXI.loaders.Loader();
            requestToLoadUnencryptedPixiImages(instructionFile, instructionFileName, loadPatch, loadTarget);
                      
          }, 1000);
        } else {
          //request to load stuff
          let resources = await loadUnencryptedPixiImages(instructionFile, loadPatch, loadTarget);  // Await the image loading
          if (showlog(CONSOLELOG.IMG_LOADING)) {console.log("For \"" + instructionFile.name + "\" PIXI has loaded resources: ", resources);}
          let numberOfKeysProcessed = 0;
          let numberOfKey = Object.keys(resources).length;
          for (key in resources){
            numberOfKeysProcessed += 1;
            let resImgData = getImgData(resources, key);
            if (canopyIB.do.outputLoadedImagesInDebugFolderIfInDevEnvironment && canopyIB.isInDevEnvironment){
              outputPath = canopyIB.paths.debug + "/loaded_assets/" + instructionFileName + "_loaded_" + key + ".png";
              let reconstructedBmpCanvas = generateCanvasWithImageDataData (resImgData.data, resImgData.width, resImgData.height);
              exportCanvasAsImgFile(reconstructedBmpCanvas, outputPath);
            }
            resources[key].texture.destroy(true);
            if (numberOfKeysProcessed == numberOfKey){
              canopyIB.pixiLoader.destroy();
            }
            addImgDataToInstructionFiles(instructionFileName, key, resImgData);
          }
        } 
    } catch (error) {
      console.error("Error in requestToLoadUnencryptedPixiImages:", error);
    }

    function getImgData(resources, resourceLabel){
      let resourceTextureBaseTexture = resources[resourceLabel].texture.baseTexture
      let width = resourceTextureBaseTexture.width;
      let height = resourceTextureBaseTexture.height;
      let canvas = document.createElement('canvas');
      let context = canvas.getContext('2d');
      canvas.width = width;
      canvas.height = height;
      context.clearRect(0, 0, width, height);
      context.drawImage(resourceTextureBaseTexture.source, 0, 0);
      let imgData = context.getImageData(0, 0, canvas.width, canvas.height);
      return imgData;
    }
  }

  async function loadUnencryptedPixiImages(instructionFile, loadPatch, loadTarget) {
    loadPatch = loadPatch || false;
    loadTarget = loadTarget || false;
    
    let inputPath = canopyIB.paths.input;
    if (canopyIB.isInDevEnvironment){
      inputPath = canopyIB.paths.inputDev;
    }
    
    canopyIB.pixiLoader = new PIXI.loaders.Loader();
    if (instructionFile.masks != null){
      for (let maskPathKey in instructionFile.masks){
        canopyIB.pixiLoader.add(maskPathKey, (inputPath + "/" + instructionFile.masks[maskPathKey]));
      };
    }
    if ( (instructionFile.patch != null) && (loadTarget) ){
      canopyIB.pixiLoader.add(instructionFile.name + "_target", (canopyIB.paths.targets + "/" + instructionFile.patch["targetFileName"]));
    }

    return new Promise((resolve, reject) => {
      canopyIB.pixiLoader.load((pixiLoader, resources) => {
            if (resources) {
                resolve(resources);
            } else {
                reject(new Error("Failed to load resources"));
            }
        });
    });
  }

  function addImgDataToInstructionFiles(instructionFileName, imgName, imgData){
    canopyIB.instructionFiles[instructionFileName].imgData[imgName] = imgData;
    let allImgDataLoaded = true;
    for (let key in canopyIB.instructionFiles[instructionFileName].imgData){
      if (canopyIB.instructionFiles[instructionFileName].imgData[key] == null){
        allImgDataLoaded = false;
      }
    }
    if (allImgDataLoaded) {
      processInstructionFile(canopyIB.instructionFiles[instructionFileName], instructionFileName);
    }
  }

  function copyFileFromDevInputFolderToRegularInputfolderIfIsInDevEnvSync(fileNameWithExtension){
    if (canopyIB.isInDevEnvironment) {
      try {
        fs.copyFileSync(canopyIB.paths.inputDev + "/" + fileNameWithExtension, canopyIB.paths.input + "/" + fileNameWithExtension);
      } catch (err) {
          console.error('Error occurred while copying the file:', err);
      }
    }
  }

  function requestToProcessNextInstructionFile(){
    let allInstructionFilesHaveBeenScheduledToBeProcessed = true;
    let instructionFileName;
    for (let index in canopyIB.instructionFileNames){
      let fileName = canopyIB.instructionFileNames[index];
      if (fileName != null){
        instructionFileName = fileName;
        canopyIB.instructionFileNames[index] = null;
        allInstructionFilesHaveBeenScheduledToBeProcessed = false;
        break
      }
    }
    if (!allInstructionFilesHaveBeenScheduledToBeProcessed){
      let instructionFile = canopyIB.instructionFiles[path.parse(instructionFileName).name];
      if (showlog(CONSOLELOG.IMG_LOADING)) {console.log("request to process instructionFile \"" + instructionFile.name + "\": ", instructionFile)};
      //load encrypted original files needed to create new pictures.
      for (let imagePathKey in instructionFile.sourceImages){
        let bmpFilePath = canopyIB.paths.folderWithEncryptedAssets + instructionFile.sourceImages[imagePathKey];
        let folderName = path.dirname(bmpFilePath) + "/";
        let bmpFileName = path.basename(bmpFilePath).slice(0,-4);
        let mvBitmap = ImageManager.loadBitmap(folderName, bmpFileName);
        mvBitmap.addLoadListener(onLoadBitmapMV.bind(this, path.parse(instructionFileName).name, imagePathKey));
      }
      requestToLoadUnencryptedPixiImages(instructionFile, path.parse(instructionFileName).name, instructionFile.loadPatch, instructionFile.loadTarget);
    } else {
      console.log("All instruction files have already been scheduled to be processed.");
    }
  }

  function processInstructionFile(instructionFile, instructionFileName) {
    try {
        //schedule the next instructionFile for Execution
        let thisIsTheLastInstructionFile = true;
        for (let currFileName of canopyIB.instructionFileNames){
          if ( currFileName != null){
            thisIsTheLastInstructionFile = false;
            requestToProcessNextInstructionFile()
            break
          }
        }
        //process instruction file
        if (showlog(CONSOLELOG.DEBUG)) {console.log("processing instructionFile \"" + instructionFileName + "\":", instructionFile);};

        loadPatch = instructionFile.loadPatch;
        loadTarget = instructionFile.loadTarget;
        let outputFileNameWithoutExtension = instructionFileName;
        let pixiApp = new PIXI.Application({
          width: instructionFile.imgSize.width,
          height: instructionFile.imgSize.height,
          transparent: true
        });
        let pixiContainer = new PIXI.Container();
        let newTexture = PIXI.RenderTexture.create(
          instructionFile.imgSize.width,
          instructionFile.imgSize.height
        );

        function calcPixiTexture(imgData, offsetX, offsetY, width, height){
          offsetX = offsetX || 0;
          offsetY = offsetY || 0;
          width = width || imgData.width;
          height = height || imgData.height;
          let spriteRect = new PIXI.Rectangle(offsetX, offsetY, width, height);
          let tmpCanvas = generateCanvasWithImageDataData(imgData.data,imgData.width, imgData.height);
          let baseTexture = PIXI.BaseTexture.fromCanvas(tmpCanvas);
          return new PIXI.Texture(baseTexture, spriteRect);
        }

        let instImgData = canopyIB.instructionFiles[instructionFileName].imgData;
        for (let currentSectionKey in instructionFile.sections){
          let currentSection = instructionFile.sections[currentSectionKey];
          let maskTexture;
          let croppedTexture;
          let maskSprite;
          let spriteClone;
          let transformMatrix;
          let maskBoundingRect;
          let colorAdjustments;
          let alphaChange;
          let blendMode;
          for (let index in currentSection){
            let instruction = currentSection[index];
            if (instruction.action.toUpperCase() === "SELECT_RECT"){
              instruction.x = instruction.x || 0;
              instruction.y = instruction.y || 0;
              instruction.width = instruction.width || instImgData[instruction.imgName].width;
              instruction.height = instruction.height || instImgData[instruction.imgName].height;

              croppedTexture = calcPixiTexture(instImgData[instruction.imgName], instruction.x, instruction.y, instruction.width, instruction.height);
              spriteClone = new PIXI.Sprite(croppedTexture);
              maskSprite = null;
              transformMatrix = new PIXI.Matrix();
              colorAdjustments = null;
              alphaChange = null;
              blendMode = null;
              maskBoundingRect = null;
            } else if (instruction.action.toUpperCase() === "SELECT_MASK"){
              /* I can't make sense of how masks behave when they're attached to their sprite in combination with cropping.
              * TODO: figure out how to use masks properly or perhaps render an image to process it further...
              * ... or be lazy and just work with the system as it is right now.
              * 
              * For now lets just project the entire image and the offset of the image to subtract it during paste
              */
              maskBoundingRect = calcBoundingRect(instImgData[instruction.maskName]);

              croppedTexture = calcPixiTexture(instImgData[instruction.imgName], 0, 0, instImgData[instruction.imgName].width, instImgData[instruction.imgName].height);
              spriteClone = new PIXI.Sprite(croppedTexture);
              maskTexture = calcPixiTexture(instImgData[instruction.maskName], 0, 0, instImgData[instruction.maskName].width, instImgData[instruction.maskName].height);
              maskSprite = new PIXI.Sprite(maskTexture);
              
              spriteClone.addChild(maskSprite);
              spriteClone.mask = maskSprite;

              transformMatrix = new PIXI.Matrix();
              colorAdjustments = null;
              alphaChange = null;
              blendMode = null;
            } else if (instruction.action.toUpperCase() === "PASTE"){
              let pasteTransformMatrix = new PIXI.Matrix();
              pasteTransformMatrix.append(transformMatrix)
              let projectionOffset = calcProjectionOffset(pasteTransformMatrix, croppedTexture.width, croppedTexture.height);
              let projectedMaskOffset = new PIXI.Point(0,0);
              if (maskBoundingRect != null){
                let maskOffset = new PIXI.Point (maskBoundingRect.x, maskBoundingRect.y);
                if (projectionOffset.isFlippedHorizontally){
                  maskOffset.x = maskBoundingRect.x + maskBoundingRect.width - croppedTexture.width;
                  //console.log("maskIsFlippedHorizontally");
                }
                if (projectionOffset.isFlippedVertically){
                  maskOffset.y = maskBoundingRect.y + maskBoundingRect.height - croppedTexture.height;
                  //console.log("maskIsFlippedVertically");
                }
                //console.log("maskOffset: ", maskOffset);
                projectedMaskOffset = pasteTransformMatrix.apply(maskOffset);
                //console.log("projectedMaskOffset: ", projectedMaskOffset);
              }
              instruction.x = instruction.x || 0;
              instruction.y = instruction.y || 0;
              pasteTransformMatrix.tx += instruction.x - projectionOffset.x - projectedMaskOffset.x;
              pasteTransformMatrix.ty += instruction.y - projectionOffset.y - projectedMaskOffset.y;
              spriteClone.transform.setFromMatrix(pasteTransformMatrix);
              if (maskSprite != null) {
                //spriteClone.mask = maskSprite.transform.setFromMatrix(pasteTransformMatrix);
              }
              if (colorAdjustments != null){
                let colorMatrixHue = new PIXI.filters.ColorMatrixFilter ();
                let colorMatrixSaturate = new PIXI.filters.ColorMatrixFilter ();
                let colorMatrixBrightness = new PIXI.filters.ColorMatrixFilter ();
                colorMatrixSaturate.saturate(colorAdjustments.saturation, false);
                colorMatrixHue.hue(colorAdjustments.hue, false);
                colorMatrixBrightness.brightness(colorAdjustments.brightness, false);
                spriteClone.filters = [colorMatrixHue, colorMatrixSaturate, colorMatrixBrightness];
              }
              if (alphaChange != null){
                spriteClone.alpha = spriteClone.alpha * alphaChange;
              }       
              if (blendMode != null){
                spriteClone.blendMode = blendMode;
              }    
              pixiContainer.addChild(spriteClone);
              spriteClone = new PIXI.Sprite(croppedTexture);
              if (maskSprite != null) {
                maskSprite = new PIXI.Sprite(maskTexture);
                spriteClone.addChild(maskSprite);
                spriteClone.mask = maskSprite;
              } 
            } else if (instruction.action.toUpperCase() === "RESET_PROJECTIONS"){
              spriteClone = new PIXI.Sprite(croppedTexture);
              transformMatrix = new PIXI.Matrix();
              colorAdjustments = null;
              if (maskSprite != null) {
                maskSprite = new PIXI.Sprite(maskTexture);
                spriteClone.addChild(maskSprite);
                spriteClone.mask = maskSprite;
              } 
            }else if (instruction.action.toUpperCase() === "TRANSFORM"){
              transformMatrix.append(createPixiMatrix(instruction.matrix));
              //projectionOffset.x -= instruction.matrix[2];
              //projectionOffset.y -= instruction.matrix[5];
            }else if (instruction.action.toUpperCase() === "FLIP"){
                if (instruction.direction.toLowerCase() === "horizontal"){
                  transformMatrix.append(createPixiMatrix([-1,0,0, 0,1,0]));
                  //projectionOffset.x += spriteClone.width;
                }
                else if (instruction.direction.toLowerCase() === "vertical"){
                  transformMatrix.append(createPixiMatrix([1,0,0, 0,-1,0]));
                  //projectionOffset.y += spriteClone.height;
                }
                else { throw new Error("FLIP direction is neither \"horizontal\" nor \"vertical\", but: " + instruction.action); }
            } else if (instruction.action.toUpperCase() === "SCALE"){
                transformMatrix.append(createPixiMatrix([ instruction.scalefactor,0,0, 0,instruction.scalefactor,0]));
            } else if (instruction.action.toUpperCase() === "ADJUST_HSL") {
              let gimpH = instruction.hue || 0;
              let gimpS = instruction.saturation || 0;
              let gimpL = instruction.lightness || 1;
              // colorMatrix.hue is in degrees here and in GIMP
              // saturation is used outside the range of the documentation 
              // GIMP saturation [-100, 100] is about [-0.9, 0.9] colorMatrix.saturate
              // colorMatrix.brightness is bizarre and seems to change both saturation and brightness of the GIMP HSL model
              // a change of colorMatrix.brightness is roughly estimated to correlate with 172 GIMP lightness and 139 GIMP saturation
              // => a change of 100 GIMP lightness should be roughly equivalent to 0.581 colorMatrix.brightness and - 0.72 colorMatrix.saturate
              colorAdjustments = {
                hue: gimpH, //in degrees here and in GIMP
                saturation: (0 + 0.9*(gimpS/100) - 0.72*(gimpL/100)), //(instruction.saturation/100), //[-1,1] or [-.9,.9] range aproximates GIMP [-100,100]; (ammount[0-1], multiply) here according to docu, [-1,1] seems to work though;  -100 to 100 in GIMP (input);
                brightness: (1 + 0.581*(gimpL/100)), //(ammount[0-1], multiply) here according to docu, [0,2] seems to work though,1 seems to be unmodified;  -100 to 100 in GIMP (input)
              }
            } else if (instruction.action.toUpperCase() === "ALPHA") {
              alphaChange = instruction.multiplier;
            } else if (instruction.action.toUpperCase() === "BLEND_MODE"){
              blendMode = PIXI.BLEND_MODES[instruction.blendMode.toUpperCase()];
            } else {
              throw new Error("unknown action: " + instruction.action);
            }
          }
        }

        function calcProjectionOffset(pixiMatrix, selectionWidth, selectionHeight){
          let selectionCorners = [
            new PIXI.Point(0,0),
            new PIXI.Point(selectionWidth,0),
            new PIXI.Point(0,selectionHeight),
            new PIXI.Point(selectionWidth,selectionHeight)
          ]
          projectedCorners = [];
          for (let pixiPoint of selectionCorners){
            projectedCorners.push(pixiMatrix.apply(pixiPoint));
          }
          let isFlippedHorizontally = false;
          let isFlippedVertically = false;
          let minX = null;
          let minY = null;
          for (let index in projectedCorners){
            let pixiPoint = projectedCorners[index];
            if (minX == null){
              minX = pixiPoint.x;
              minY = pixiPoint.y;
            } else {
              if (pixiPoint.x < minX) {
                minX = pixiPoint.x;
                if ((index == 1)  || index == 3){
                  isFlippedHorizontally = true;
                } else {
                  isFlippedHorizontally = false;
                }
              }
              if (pixiPoint.y < minY) { 
                minY = pixiPoint.Y;
                if ((index == 2)  || index == 3){
                  isFlippedVertically = true;
                } else {
                  isFlippedHorizontally = false;
                }
              }
            }
          }
          let pixiPoint = new PIXI.Point(Math.round(minX), Math.round(minY));
          pixiPoint.isFlippedHorizontally = isFlippedHorizontally;
          pixiPoint.isFlippedVertically = isFlippedVertically;
          return (pixiPoint);
        }

        pixiApp.renderer.render(pixiContainer, newTexture);
        let baseCanvas = pixiApp.renderer.extract.canvas(newTexture);
        if (canopyIB.isInDevEnvironment){
          outputPath = "" + canopyIB.paths.debug + "/" + outputFileNameWithoutExtension + "_base.png";
          exportCanvasAsImgFile(baseCanvas, outputPath);
        }
        //*
        pixiApp.destroy(true, {
          children: true,
          texture: true,
          baseTexture: true
        })
        //*/

        //calculate patch
        if (canopyIB.do.calculatePatches && canopyIB.isInDevEnvironment && instructionFile.loadTarget) {

          let baseImageData = baseCanvas.getContext("2d").getImageData(0,0,baseCanvas.width,baseCanvas.height);
          let targetImageData = instImgData[instructionFile.name + "_target"];//getImgData(resources, "targetFileName");

          let patchData = new Uint8Array(baseImageData.data.length);
          let isPatchedData = new Uint8ClampedArray(baseImageData.data.length);
          let pseudoPatchImgData = new Uint8ClampedArray(baseImageData.data.length);

          //let cnt = 0;
          for (let i = 0; i < baseImageData.data.length; i += 4) {
            patchData[i]     = targetImageData.data[i]     - baseImageData.data[i];     // red
            patchData[i + 1] = targetImageData.data[i + 1] - baseImageData.data[i + 1]; // green
            patchData[i + 2] = targetImageData.data[i + 2] - baseImageData.data[i + 2]; // blue
            patchData[i + 3] = targetImageData.data[i + 3] - baseImageData.data[i + 3]; // alpha
            //patchData[i + 3] = 255;
            
            if (patchData[i] != 0 || patchData[i + 1] != 0 || patchData[i + 2] != 0 || patchData[i + 3] != 0){
              isPatchedData[i] = 255;
              isPatchedData[i + 1] = 255;
              isPatchedData[i + 2] = 255;
              isPatchedData[i + 3] = 255;

              pseudoPatchImgData[i] = patchData[i];
              pseudoPatchImgData[i + 1] = patchData[i + 1];
              pseudoPatchImgData[i + 2] = patchData[i + 2];
              pseudoPatchImgData[i + 3] = 255;
              /*
              if (baseImageData.data[i + 3] > 250 && targetImageData.data[i + 3] < 50){
                cnt += 1;
                //if a pixel has been hidden the original pixel is displayed with low opacity
                pseudoPatchImgData[i] = baseImageData.data[i];
                pseudoPatchImgData[i + 1] = baseImageData.data[i + 1];
                pseudoPatchImgData[i + 2] = baseImageData.data[i + 2];
                pseudoPatchImgData[i + 3] = 50;
              }
              //*/
            } else {
              isPatchedData[i] = 0;
              isPatchedData[i + 1] = 0;
              isPatchedData[i + 2] = 0;
              isPatchedData[i + 3] = 255;

              pseudoPatchImgData[i] = 0;
              pseudoPatchImgData[i + 1] = 0;
              pseudoPatchImgData[i + 2] = 0;
              pseudoPatchImgData[i + 3] = 0;
            }
          }
          //console.log("pixels hidden in " + instructionFileName +"-patch: ", cnt);
          
          //export patch as bin.gz
          outputPath = canopyIB.paths.input + "/" + instructionFile.patch["patchFileName"];
                      
          exportPatchAsBinGz(patchData, outputPath);
          //dev exports
          outputPath = canopyIB.paths.inputDev + "/" + instructionFile.patch["patchFileName"];
          exportPatchAsBinGz(patchData, outputPath);
          outputPath = canopyIB.paths.debug + "/" + instructionFileName + "_isPatched.png";
          let isPatchedCanvas = generateCanvasWithImageDataData (isPatchedData, baseCanvas.width, baseCanvas.height)
          exportCanvasAsImgFile(isPatchedCanvas, outputPath);
          outputPath = canopyIB.paths.debug + "/" + instructionFileName + "_pseudoPatchImg.png";
          let pseudoPatchCanvas = generateCanvasWithImageDataData (pseudoPatchImgData, baseCanvas.width, baseCanvas.height)
          exportCanvasAsImgFile(pseudoPatchCanvas, outputPath);
        }
        let outputCanvas;
        //Apply patch to baseImage
        if (canopyIB.do.applyPatches && (loadPatch || canopyIB.do.calculatePatches && loadTarget)) {
          //Create output image by combining base image with patch
          //let patchPath = canopyIB.paths.inputDev + "/" + outputFileNameWithoutExtension + "_patch.bin.gz";
          let patchPath = "" + canopyIB.paths.input + "/" + instructionFile.patch["patchFileName"];
          let loadedPatchData;
          try {
            let compressedData = fs.readFileSync(patchPath);
            let decompressedData = zlib.gunzipSync(compressedData);
            loadedPatchData = new Uint8Array(decompressedData);
          } catch (err) {
              console.error('Error:', err);
          }
          
          let baseImageData = baseCanvas.getContext("2d").getImageData(0,0,baseCanvas.width,baseCanvas.height);
          let combinedData = new Uint8Array(baseImageData.data.length);
          for (let i = 0; i < baseImageData.data.length; i += 4) {
            combinedData[i]     = loadedPatchData[i]     + baseImageData.data[i];     // Red
            combinedData[i + 1] = loadedPatchData[i + 1] + baseImageData.data[i + 1]; // Green
            combinedData[i + 2] = loadedPatchData[i + 2] + baseImageData.data[i + 2]; // Blue
            combinedData[i + 3] = loadedPatchData[i + 3] + baseImageData.data[i + 3]; // Alpha
          }

          outputCanvas = generateCanvasWithImageDataData (new Uint8ClampedArray(combinedData), baseCanvas.width, baseCanvas.height)
        } else {
          //no patch is needed. Base image is final output image
          outputCanvas = baseCanvas;
        }
        //save output image
        outputPath = "" + canopyIB.paths.output + "/" + outputFileNameWithoutExtension + ".png";
        exportCanvasAsImgFile(outputCanvas, outputPath);
        /*
        if (canopyIB.isMod){
          outputPath = "" + canopyIB.paths.output + "/" + outputFileNameWithoutExtension + ".k9a";
          exportCanvasAsImgFile(outputCanvas, outputPath);
        }
        */
        if (canopyIB.isInDevEnvironment){
          outputPath = "" + canopyIB.paths.outputDev + "/" + outputFileNameWithoutExtension + ".png";
          exportCanvasAsImgFile(outputCanvas, outputPath);
          let outputFileInGameImgDirPath = "" + canopyIB.paths.projectRoot + instructionFile.outputDestination + outputFileNameWithoutExtension + ".png";
          if (canopyIB.do.saveOutputInGameImgFolder){
            exportCanvasAsImgFile(outputCanvas, outputFileInGameImgDirPath);
          }
          //delete img file in original game image dir /project/img/...
          //move file deletion to bundling tool
          /*
          if (canopyIB.do.deleteOutputInGameImgFolder){
            fs.access(outputFileInGameImgDirPath, fs.constants.F_OK, (err) => {
              if (!err) {
                fs.unlink(outputFileInGameImgDirPath, (err) => {
                  if (err) {
                    console.error(`Error deleting file: ${err.message}`);
                  } else {
                    console.log(`File deleted successfully: ` + outputFileInGameImgDirPath);
                  }
                });
              } else {
                //file doesn't exist
              }
            });
          }
          //*/
        }
        
        if (thisIsTheLastInstructionFile){
          console.log("CanopyImageBuilder has processed all instruction files.");
          canopyIB.BitmapRequestData.imgagesCanBeLoaded = true;
          //TODO clean up!
        }
    } catch (error) {
        console.error('Error loading images:', error);
    }
  }

})();